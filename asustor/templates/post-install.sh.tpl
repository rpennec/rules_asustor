#!/bin/sh
echo "post-install"
docker pull {IMAGE}:{TAG}

CONTAINER_TEST=$(docker container ls -a | awk '/{NAME}/ {print $1}')

if [ ! -z $CONTAINER_TEST ]; then
	docker rm -f $CONTAINER_TEST
fi

docker create -i -t --name={NAME} \
	--restart=unless-stopped \
	{PORTS_MAP} {VOLUMES_MAP} {IMAGE} {COMMAND}

case "$APKG_PKG_STATUS" in
	install)
		;;
	upgrade)
		oldim=$(docker images | grep {IMAGE} | grep none | awk '{print $3}')
		echo $oldim

		if [ ! -z $oldim ]; then
			docker rmi -f $oldim
		fi
		;;
	*)
		;;
esac
exit 0
