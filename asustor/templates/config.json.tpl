{
   "general":{
      "package":"{PKG_NAME}",
      "name":"{NAME}",
      "version":"{VERSION}",
      "depends":[
         "docker-ce(>=18.03.1.r1)"
      ],
      "conflicts":[],
      "developer":"{DEVELOPER}",
      "maintainer":"{MAINTAINER}",
      "email":"{EMAIL}",
      "website":"{WEBSITE}",
      "architecture":"x86-64",
      "firmware":"3.0.0"
      },
      "adm-desktop": {
			"app": {
				"port": {PORT},
				"protocol": "http",
				"type": "custom",
				"url": "/" 
			},
			"privilege": {
				"accessible": "users", 
				"customizable": true 
			} 
                 
      },
      "register":{
			"share-folder":[
            {SHARE_FOLDERS}
			],
			"boot-priority":{
				"start-order":60,
				"stop-order":40
			},
            "port":[
             {PORTS}
            ]
      }
}
