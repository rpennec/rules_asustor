#!/bin/sh -e

case "$1" in
    start)
        echo "Start {NAME} container ..."
        docker start {NAME}
        sleep 10
        ;;
    stop)
    	echo "Stop {NAME} container ..."
    	docker stop {NAME}
		sleep 5
        ;;
    reload)
    	echo "Reload {NAME} container ..."
    	docker stop {NAME}
    	sleep 5
    	docker start {NAME}
        ;;
    *)
        echo "Usage: $0 {start|stop|reload}"
        exit 1
        ;;
esac
exit 0
