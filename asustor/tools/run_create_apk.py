#!/usr/bin/env python3
import os
import sys
from pathlib import Path
import subprocess
import tarfile

control_tar_file = Path(sys.argv[1])
output_file = Path(sys.argv[2])

with tarfile.open(control_tar_file, "r") as tar:
    tar.extractall(output_file.stem)

return_code = subprocess.call(
    args=[
        "asustor/tools/apkg-tools.py",
        "create",
        output_file.stem,
        "--destination",
        output_file.stem,
    ]
)

if return_code != 0:
    sys.exit(return_code)

apks = [f for f in os.listdir(output_file.stem) if f.endswith(".apk")]
generated = Path(output_file.stem, apks[0])

os.rename(generated, output_file)
