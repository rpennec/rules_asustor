"""Bazel rules for building ASUSTOR APK files
"""

load("@rules_pkg//:pkg.bzl", "pkg_tar")

def asustor_apk(name, visibility = None, **kwargs):
    tar_target = "_{}_tar".format(name)
    asustor_control_tar(
        name = tar_target,
        **kwargs
    )
    _asustor_apk(
        name = name,
        control_tar = ":{}".format(tar_target),
        apk = "{}.apk".format(name),
    )

def _asustor_apk_impl(ctx):
    ctx.actions.run(
        executable = ctx.executable._create_exec,
        arguments = [ctx.file.control_tar.path, ctx.outputs.apk.path],
        inputs = ctx.files._create_exec + ctx.files.control_tar,
        outputs = [ctx.outputs.apk],
    )

_asustor_apk = rule(
    implementation = _asustor_apk_impl,
    attrs = {
        "control_tar": attr.label(mandatory = True, allow_single_file = ["tar"]),
        "_create_exec": attr.label(executable = True, cfg = "exec", allow_files = True, default = Label("//asustor/tools:run_create_apk")),
        "apk": attr.output(mandatory = True),
    },
)

def _share_folders_str(apk_name):
    _folder_str = """
             {
                "name":"%s",
                "description":"%s"
             }
"""
    folders_dict = {"{}_data".format(apk_name.lower()): "{} data volume".format(apk_name)}
    return ", ".join([_folder_str % (name, desc) for name, desc in folders_dict.items()])

def _docker_ports_str(ports):
    return " ".join(["-p {host}:{container}".format(host = p, container = p) for p in ports])

def _docker_shared_volumes_cli(shared_folder):
    return _docker_volumes_cli({
        "/volume1/{}/data".format(shared_folder): "/data",
    }) if shared_folder else ""

def _docker_volumes_cli(volumes_map):
    return " ".join([
        "-v {}:{}".format(h, c)
        for h, c in volumes_map.items()
    ])

def _asustor_control_files_impl(ctx):
    out_dir = ctx.attr.apk_name
    outputs = []
    image, tag = ctx.attr.docker_image.split(":")
    shared_folder = ctx.attr.apk_name.lower() + "_data" if ctx.attr.shared_folder else None
    docker_volumes_cli = _docker_volumes_cli(ctx.attr.volumes_map) if ctx.attr.volumes_map else _docker_shared_volumes_cli(shared_folder)

    # create changelog.txt
    out = ctx.actions.declare_file(out_dir + "/changelog.txt")
    ctx.actions.write(
        output = out,
        content = "update to version {}\n\n".format(ctx.attr.apk_version),
    )
    outputs.append(out)

    # create config.json
    out = ctx.actions.declare_file(out_dir + "/config.json")
    ctx.actions.expand_template(
        output = out,
        template = ctx.file._config_tpl,
        substitutions = {
            "{PKG_NAME}": ctx.attr.apk_name,
            "{NAME}": ctx.attr.apk_name.lower(),
            "{VERSION}": ctx.attr.apk_version,
            "{DEVELOPER}": ctx.attr.apk_name.capitalize(),
            "{MAINTAINER}": ctx.attr.maintainer,
            "{EMAIL}": ctx.attr.email,
            "{WEBSITE}": ctx.attr.website,
            "{PORT}": str(ctx.attr.ports[0]),
            "{PORTS}": ", ".join(['"{}"'.format(p) for p in ctx.attr.ports]),
            "{SHARE_FOLDERS}": _share_folders_str(ctx.attr.apk_name) if shared_folder else "",
        },
        is_executable = False,
    )
    outputs.append(out)

    # create description.txt
    out = ctx.actions.declare_file(out_dir + "/description.txt")
    ctx.actions.write(
        output = out,
        content = "{}\n{}\n".format(ctx.attr.apk_name, ctx.attr.description),
    )
    outputs.append(out)

    # create icon.png
    if ctx.attr.icon:
        out = ctx.actions.declare_file(out_dir + "/icon.png")
        ctx.actions.run_shell(
            outputs = [out],
            inputs = [ctx.file.icon],
            command = "cp '%s' '%s'" % (ctx.file.icon.path, out.path),
        )
        outputs.append(out)

    # create post-install.sh
    out = ctx.actions.declare_file(out_dir + "/post-install.sh")
    ctx.actions.expand_template(
        output = out,
        template = ctx.file._post_install_tpl,
        substitutions = {
            "{IMAGE}": image,
            "{TAG}": tag,
            "{NAME}": ctx.attr.apk_name,
            "{COMMAND}": ctx.attr.docker_command,
            "{PORTS_MAP}": _docker_ports_str(ctx.attr.ports),
            "{VOLUMES_MAP}": docker_volumes_cli,
        },
        is_executable = True,
    )
    outputs.append(out)

    # create post-uninstall.sh
    out = ctx.actions.declare_file(out_dir + "/post-uninstall.sh")
    ctx.actions.expand_template(
        output = out,
        template = ctx.file._post_uninstall_tpl,
        substitutions = {},
        is_executable = True,
    )
    outputs.append(out)

    # create pre-install.sh
    out = ctx.actions.declare_file(out_dir + "/pre-install.sh")
    ctx.actions.expand_template(
        output = out,
        template = ctx.file._pre_install_tpl,
        substitutions = {
            "{IMAGE}": image,
        },
        is_executable = True,
    )
    outputs.append(out)

    # create pre-uninstall.sh
    out = ctx.actions.declare_file(out_dir + "/pre-uninstall.sh")
    ctx.actions.expand_template(
        output = out,
        template = ctx.file._pre_uninstall_tpl,
        substitutions = {
            "{IMAGE}": image,
        },
        is_executable = True,
    )
    outputs.append(out)

    # create start-stop.sh
    out = ctx.actions.declare_file(out_dir + "/start-stop.sh")
    ctx.actions.expand_template(
        output = out,
        template = ctx.file._start_stop_tpl,
        substitutions = {
            "{NAME}": ctx.attr.apk_name,
        },
        is_executable = True,
    )
    outputs.append(out)

    return [DefaultInfo(files = depset(outputs))]

asustor_control_files = rule(
    implementation = _asustor_control_files_impl,
    attrs = {
        "apk_name": attr.string(mandatory = True),
        "apk_version": attr.string(mandatory = True),
        "description": attr.string(),
        "docker_image": attr.string(mandatory = True),
        "docker_command": attr.string(),
        "maintainer": attr.string(),
        "email": attr.string(),
        "website": attr.string(),
        "ports": attr.int_list(allow_empty = False),
        "shared_folder": attr.bool(),
        "volumes_map": attr.string_dict(),
        "icon": attr.label(allow_single_file = [".png"]),
        "_config_tpl": attr.label(default = ":templates/config.json.tpl", allow_single_file = True),
        "_post_install_tpl": attr.label(default = ":templates/post-install.sh.tpl", allow_single_file = True),
        "_post_uninstall_tpl": attr.label(default = ":templates/post-uninstall.sh.tpl", allow_single_file = True),
        "_pre_install_tpl": attr.label(default = ":templates/pre-install.sh.tpl", allow_single_file = True),
        "_pre_uninstall_tpl": attr.label(default = ":templates/pre-uninstall.sh.tpl", allow_single_file = True),
        "_start_stop_tpl": attr.label(default = ":templates/start-stop.sh.tpl", allow_single_file = True),
    },
)

def asustor_control_tar(name, **kwargs):
    control_target = "_{}_control".format(name)
    asustor_control_files(
        name = control_target,
        **kwargs
    )
    pkg_tar(
        name = name,
        srcs = [":" + control_target],
        package_dir = "CONTROL",
        extension = "tar",
    )
