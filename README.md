# Bazel rules for ASUSTOR Packages

Bazel rules for building ASUSTOR Packages, known as APK files, to sideload apps onto your NAS.

See [examples](/examples/).

_App Central_ `->` _Management_ `->` _Manual Install_

![App Central Manual Install](examples/app_central.png)
